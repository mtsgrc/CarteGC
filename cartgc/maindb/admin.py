from django.contrib import admin
from cartgc.maindb.models import MaindbCartel, MaindbMensaje, MaindbBrillo, MaindbTiempomuestra, MaindbTipofont, MaindbTiposcroll

# Register your models here.

class CartelAdmin(admin.ModelAdmin):
    list_display = ['namecartel','id','lastlogin','lastbrightness','lastmessages','lastbroadcasting','lastreading','laststatus','lasterrors',]
    list_filter = ['lastlogin','lastreading',]
    ordering = ['id','-lastlogin',]

class MensajeAdmin(admin.ModelAdmin):
    list_display = ['idmessage','contentmessage','typescrolling','timemessage','typefont',]
    list_filter = ['typescrolling','timemessage','typefont',]
    ordering = ['idmessage',]
    search = ['contentmessage',]

class BrilloAdmin(admin.ModelAdmin):
    list_display = ['percentbrightness','valuebrightness',]
    ordering = ['percentbrightness',]

class TiempoMuestraAdmin(admin.ModelAdmin):
    list_display = ['idname','valuetime']
    ordering = ['valuetime',]

class TipoScrollAdmin(admin.ModelAdmin):
    list_display = ['idname','valuescroll',]
    ordering = ['valuescroll',]

class TipoFontAdmin(admin.ModelAdmin):
    list_display = ['idname','idfont']
    ordering = ['idfont',]

admin.site.register(MaindbCartel, CartelAdmin)
admin.site.register(MaindbMensaje, MensajeAdmin)
admin.site.register(MaindbBrillo, BrilloAdmin)
admin.site.register(MaindbTiempomuestra, TiempoMuestraAdmin)
admin.site.register(MaindbTipofont, TipoFontAdmin)
admin.site.register(MaindbTiposcroll, TipoScrollAdmin)
