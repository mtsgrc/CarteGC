from django import forms
from cartgc.maindb.models import MaindbMensaje, MaindbCartel, MaindbPackage, MaindbTask
from django.forms.models import modelform_factory
from cartgc.maindb.optcfg import *

TIPOCONTACTO_CHOICES = (
    ('general', 'General'),
    ('error', 'Reporte de error'),
    ('sugerencia', 'Sugerencia'),
)

TIPOTAREAS_CHOICES = tuple(typesTasks.items())
TIPOSTATTAREAS_CHOICES = (
    ('c', 'Completa'),
    ('i', 'Incompleta'),
    )

class ContactForm(forms.Form):
    asunto = forms.ChoiceField(choices=TIPOCONTACTO_CHOICES)
    mensaje = forms.CharField(widget=forms.Textarea())
    remitente = forms.EmailField(required=True)


class MensajeForm(forms.ModelForm):
    class Meta:
        model = MaindbMensaje
        fields = ['contentmessage', 'typescrolling', 'timemessage', 'typefont', 'descripcion', ]
        labels = {
            'contentmessage': 'Mensaje',
            'typescrolling': 'Scroll',
            'timemessage': 'Tiempo',
            'typefont': 'Fuente',
            'descripcion': 'Descripcion',
        }

class CartelForm(forms.ModelForm):
    id = forms.IntegerField(min_value=1, max_value=999, initial=1)

    class Meta:
        model = MaindbCartel
        fields = ['id','namecartel','lastmessages','lastbroadcasting','lastbrightness']
        labels = {
            'id': 'Id. de Cartel',
            'namecartel': 'Nombre de Cartel',
            'lastmessages': 'Mensajes',
            'lastbroadcasting': 'Broadcasting',
            'lastbrightness': 'Brillo',
            }

class PackageForm(forms.ModelForm):
    class Meta:
        model = MaindbPackage
        fields = ['namepackage','messages']
        labels = {
            'namepackage': 'Nombre de Paquete',
            'messages': 'Mensajes',
        }

class TaskForm(forms.ModelForm):

    task = forms.ChoiceField(choices=TIPOTAREAS_CHOICES)
    status = forms.ChoiceField(choices=TIPOSTATTAREAS_CHOICES)

    class Meta:
        model = MaindbTask
        fields = ['sign','todate','task','status']
        labels = {
            'sign': 'Cartel',
            'todate': 'Fecha destino',
            'task': 'Tipo Tarea',
            'status': 'Estado',
        }
