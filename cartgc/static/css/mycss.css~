/*
    DATA TABLES
*/

td.title {
    font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
    font-size: 14pt;
    font-weight: bold;
    background-color: #ddd;
    text-align: center;
    border-bottom: 2px #bbb solid;
    border-top: 2px #bbb solid;
    border-left: 1px #ccc solid;
    border-right: 1px #ccc solid;

}

td.label {
    vertical-align: middle;
    font-size: 12pt;
    text-align: right;
    font-weight: bold;
    background-color: #eaeaea;
    border-right: 1px #ccc dashed;
    border-left: 1px #ccc solid;
    border-bottom: 1px #ccc solid;
}


td.dataright {
    font-size: 12pt;
    text-align: left;
    vertical-align: top;
    line-height: 150%;
    border-bottom: 1px #ccc solid;
    background-color: #f6f6f6;
}

/* 
HEADER-links
*/

h1.header {
    font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
    font-size: 16pt;
    color: #aaa;
    padding: 0 6px 0 0;
    margin: 0 0 .2em 0;
}

.divheaderlinks {
    padding-top: 20px;
    padding-bottom: 10px;
    padding-left: 10px;
    margin-left: -10px;
    width: 100%;
    border-bottom: 1px #000 solid;
    border-top: 2px #666  solid;
    background-color: #f6f6f6;
    background-image: linear-gradient(transparent 50%, rgba(255,255,255,.5) 50%);
    background-size: 20px 20px;
}

/*
    BACKGROUNDS
*/


.background_01 {
    background-color: #026873;
    background-image: linear-gradient(90deg, rgba(255,255,255,.07) 50%, transparent 50%),
    linear-gradient(90deg, rgba(255,255,255,.13) 50%, transparent 50%),
    linear-gradient(90deg, transparent 50%, rgba(255,255,255,.17) 50%),
    linear-gradient(90deg, transparent 50%, rgba(255,255,255,.19) 50%);
    background-size: 13px, 29px, 37px, 53px;
}

.background_02 {
    background: linear-gradient(45deg, white 25%,
    #8b0 25%, #8b0 50%, 
    white 50%, white 75%, 
    #8b0 75%);
    background-size:100px 100px;
}

.background_03 {
    background: radial-gradient(circle, white 15%, transparent 10%),
    radial-gradient(circle, white 10%, black 10%) 10px 10px;
    background-size:10px 10px;
}

.backg_rombos {
    background-color: #6d695c;
    background-image:
    repeating-linear-gradient(120deg, rgba(255,255,255,.1), rgba(255,255,255,.1) 1px, transparent 1px, transparent 60px),
    repeating-linear-gradient(60deg, rgba(255,255,255,.1), rgba(255,255,255,.1) 1px, transparent 1px, transparent 60px),
    linear-gradient(60deg, rgba(0,0,0,.1) 25%, transparent 25%, transparent 75%, rgba(0,0,0,.1) 75%, rgba(0,0,0,.1)),
    linear-gradient(120deg, rgba(0,0,0,.1) 25%, transparent 25%, transparent 75%, rgba(0,0,0,.1) 75%, rgba(0,0,0,.1));
    background-size: 70px 120px;
}

.backg_rombosc {
    background-color: #eee;
    background-image: linear-gradient(45deg, black 25%, transparent 25%, transparent 75%, black 75%, black), 
    linear-gradient(-45deg, black 25%, transparent 25%, transparent 75%, black 75%, black);
    background-size:60px 60px;
}

.backg_mantel {
    background-color: hsl(2, 57%, 40%);
    background-image: repeating-linear-gradient(transparent, transparent 50px, rgba(0,0,0,.4) 50px, rgba(0,0,0,.4) 53px, transparent 53px, transparent 63px, rgba(0,0,0,.4) 63px, rgba(0,0,0,.4) 66px, transparent 66px, transparent 116px, rgba(0,0,0,.5) 116px, rgba(0,0,0,.5) 166px, rgba(255,255,255,.2) 166px, rgba(255,255,255,.2) 169px, rgba(0,0,0,.5) 169px, rgba(0,0,0,.5) 179px, rgba(255,255,255,.2) 179px, rgba(255,255,255,.2) 182px, rgba(0,0,0,.5) 182px, rgba(0,0,0,.5) 232px, transparent 232px),
    repeating-linear-gradient(270deg, transparent, transparent 50px, rgba(0,0,0,.4) 50px, rgba(0,0,0,.4) 53px, transparent 53px, transparent 63px, rgba(0,0,0,.4) 63px, rgba(0,0,0,.4) 66px, transparent 66px, transparent 116px, rgba(0,0,0,.5) 116px, rgba(0,0,0,.5) 166px, rgba(255,255,255,.2) 166px, rgba(255,255,255,.2) 169px, rgba(0,0,0,.5) 169px, rgba(0,0,0,.5) 179px, rgba(255,255,255,.2) 179px, rgba(255,255,255,.2) 182px, rgba(0,0,0,.5) 182px, rgba(0,0,0,.5) 232px, transparent 232px),
    repeating-linear-gradient(125deg, transparent, transparent 2px, rgba(0,0,0,.2) 2px, rgba(0,0,0,.2) 3px, transparent 3px, transparent 5px, rgba(0,0,0,.2) 5px);
}


.backg_gradientt {
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2VlZWVlZSIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjIwJSIgc3RvcC1jb2xvcj0iI2VlZWVlZSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9Ijk5JSIgc3RvcC1jb2xvcj0iI2VlZWVlZSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNlZWVlZWUiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(left, rgba(238,238,238,0) 0%, rgba(238,238,238,1) 20%, rgba(238,238,238,1) 99%, rgba(238,238,238,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(238,238,238,0)), color-stop(20%,rgba(238,238,238,1)), color-stop(99%,rgba(238,238,238,1)), color-stop(100%,rgba(238,238,238,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(left, rgba(238,238,238,0) 0%,rgba(238,238,238,1) 20%,rgba(238,238,238,1) 99%,rgba(238,238,238,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(left, rgba(238,238,238,0) 0%,rgba(238,238,238,1) 20%,rgba(238,238,238,1) 99%,rgba(238,238,238,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(left, rgba(238,238,238,0) 0%,rgba(238,238,238,1) 20%,rgba(238,238,238,1) 99%,rgba(238,238,238,1) 100%); /* IE10+ */
background: linear-gradient(to right, rgba(238,238,238,0) 0%,rgba(238,238,238,1) 20%,rgba(238,238,238,1) 99%,rgba(238,238,238,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00eeeeee', endColorstr='#eeeeee',GradientType=1 ); /* IE6-8 */111
}

/* 
BOTON DEL MENU PRINCIPAL
*/


.btnMenuPpal {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #f9f9f9), color-stop(1, #e9e9e9) );
	background:-moz-linear-gradient( center top, #f9f9f9 5%, #e9e9e9 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f9f9f9', endColorstr='#e9e9e9');
	background-color:#f9f9f9;
	-webkit-border-top-left-radius:23px;
	-moz-border-radius-topleft:23px;
	border-top-left-radius:23px;

	-webkit-border-top-right-radius:0px;
	-moz-border-radius-topright:0px;
	border-top-right-radius:0px;

	-webkit-border-bottom-right-radius:0px;
	-moz-border-radius-bottomright:0px;
	border-bottom-right-radius:0px;

	-webkit-border-bottom-left-radius:23px;
	-moz-border-radius-bottomleft:23px;
	border-bottom-left-radius:23p
x;
	text-indent:0px;
	border:1px solid #dcdcdc;
	display:inline-block;
	color:#666666;
	font-family:Georgia;
	font-size:20px;
	font-weight:bold;
	font-style:normal;
	height:26px;
	line-height:26px;
	width:145px;
	text-decoration:none;
	text-align:center;
	text-shadow:3px 3px 0px #ffffff;
}
.btnMenuPpal:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #e9e9e9), color-stop(1, #f9f9f9) );
	background:-moz-linear-gradient( center top, #e9e9e9 5%, #f9f9f9 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#e9e9e9', endColorstr='#f9f9f9');
	background-color:#e9e9e9;
}.btnMenuPpal:active {
	position:relative;
	top:1px;
}
