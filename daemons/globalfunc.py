#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
import os, sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CFG_DEBUG = True
CONST_PRINTABLECHARS=u"abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789,-.;:$%#@!¿¡?()_[]| ><=*" + chr(0x5c) + chr(0x2f) + chr(0x22) + chr(0x27)
typesTasks = {"b0": "Desactivar Broadcasting",
              "b1": "Activar Broadcasting",
              "m": "Enviar mensajes",
              "l": "Enviar configuracion de brillo",
              "chr": "Reiniciar cartel a modo fabrica",
              "csr": "Reiniciar cartel",
              "cce": "Borrar indicadores de error",
              "csh": "Sincronizar hora",
              "r": "Leer estado de cartel"
              }

def appendLog(idcartel,txtdescr,txtdata=""):
    dirlogs = BASE_DIR + "/logs"
    diryear = dirlogs + "/" + str(datetime.now().year)
    dirmonth = diryear + "/" + str(datetime.now().month).zfill(2)
    dirday = dirmonth + "/" + str(datetime.now().day).zfill(2)

    if os.path.isdir(dirlogs) == False:
      os.mkdir(dirlogs)

    if os.path.isdir(diryear) == False:
      os.mkdir(diryear)

    if os.path.isdir(dirmonth) == False:
      os.mkdir(dirmonth)

    if os.path.isdir(dirday) == False:
      os.mkdir(dirday)

    pathfile = dirday + "/" + str(idcartel)
    logtime = u"%s:%s:%s.%s" % (str(datetime.now().hour).zfill(2),
                               str(datetime.now().minute).zfill(2),
                               str(datetime.now().second).zfill(2),
                               str(datetime.now().microsecond / 1000).zfill(3))

    with open(pathfile, "a") as logfile:
      logfile.write((u"[%s] %s %s\n" % (logtime, txtdescr,[""," (%s)" % (txtdata)][txtdata != ""])).encode('utf-8'))

def printDEBUG(mssg):

  if CFG_DEBUG == True:
    mssghc = ""
    for c in mssg:
      if c in CONST_PRINTABLECHARS:
        mssghc += c
      else:
        mssghc += "{" + "{0:#0{1}x}".format(ord(c),4) + "}"

    print " --> --> %s" % (mssghc)
    #appendLOG("DEBUG: %s" % (mssghc))

  else:
    pass

def printOUT(mssg, stayinline=False):

    logDATETIME = datetime.now()

    mssghc = ""
    for c in mssg:
      if c in CONST_PRINTABLECHARS:
        mssghc += c
      else:
        mssghc += "{" + "{0:#0{1}x}".format(ord(c),4) + "}"


    if stayinline == True:
      textToPrint = '%02d%02d%02d.%s %s' % (logDATETIME.hour, \
                                                logDATETIME.minute, \
                                                logDATETIME.second, \
                                                str(logDATETIME.microsecond)[:3], \
                                                mssghc),
    else:
      textToPrint = '%02d%02d%02d.%s %s' % (logDATETIME.hour, \
                                                logDATETIME.minute, \
                                                logDATETIME.second, \
                                                str(logDATETIME.microsecond)[:3], \
                                                mssghc)
    print textToPrint
    #appendLOG(textToPrint)

