#!/usr/bin/env bash

# INIT SV WEB DJANGO INTERFACE

DIR_ACTUAL="$( cd `dirname "${BASH_SOURCE[0]}"` && pwd )"
DIR_ROOT="$(dirname "$DIR_ACTUAL")"

CONST_PORTWEB=8080

function getMyIPLOCAL() {
    local _ip _myip _line _nl=$'\n'
    while IFS=$': \t' read -a _line ;do
        [ -z "${_line%inet}" ] &&
           _ip=${_line[${#_line[1]}>4?1:2]} &&
           [ "${_ip#127.0.0.1}" ] && _myip=$_ip
      done< <(LANG=C /sbin/ifconfig)
    printf ${1+-v} $1 "%s${_nl:0:$[${#1}>0?0:1]}" $_myip
}

getMyIPLOCAL ipLOCALNET

python $DIR_ROOT/manage.py runserver 0.0.0.0:$CONST_PORTWEB

